package com.example.springjpamaven.controller;

import com.example.springjpamaven.entity.Users;
import com.example.springjpamaven.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping("")
    public List<Users> getUser() {
       return userService.getAllUser();
    }
    @PostMapping("")
    public void addUser(@RequestBody Users user) {
        userService.addUser(user);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable("id") String id) {
        userService.deleteUser(id);
    }

}
