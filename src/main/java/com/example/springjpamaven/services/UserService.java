package com.example.springjpamaven.services;

import com.example.springjpamaven.entity.Users;
import com.example.springjpamaven.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public List<Users> getAllUser() {
            return userRepository.findAll();
    }

    public void addUser(Users user){
        userRepository.save(user);
    }

    public void deleteUser(String id) {
        userRepository.deleteById(id);
    }
}
