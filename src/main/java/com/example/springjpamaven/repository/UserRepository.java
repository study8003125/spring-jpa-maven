package com.example.springjpamaven.repository;

import com.example.springjpamaven.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<Users, String> {
}
