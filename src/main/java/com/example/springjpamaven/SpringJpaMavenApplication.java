package com.example.springjpamaven;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringJpaMavenApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringJpaMavenApplication.class, args);
	}

}
